/**
 * 开发环境
 */
; (function () {
  window.SITE_CONFIG = {};

  // api接口请求地址，这里的88端口是网关端口,它会路由到网关微服务
  window.SITE_CONFIG['baseUrl'] = 'http://localhost:88/api';
  // window.SITE_CONFIG['baseUrl'] = 'http://localhost:8080/renren-fast';

  // cdn地址 = 域名 + 版本号
  window.SITE_CONFIG['domain'] = './'; // 域名
  window.SITE_CONFIG['version'] = '';   // 版本号(年月日时分)
  window.SITE_CONFIG['cdnUrl'] = window.SITE_CONFIG.domain + window.SITE_CONFIG.version;
})();
